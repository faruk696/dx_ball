package dxballfinal.embaded.faruk.dxballfinal;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class GameCanvas extends View {
    Paint paint;
    Brick[] bricks = new Brick[200];
    Bar bar;
    Ball ball;
    Canvas canvas;
    Result result = new Result();

    float midX=0,midY=0,barX,barY=0,maxX,maxY;
    int numBricks = 0;

    float ballX=0,ballY=0;
    boolean firstTime = true;
    private float move;
    List<Brick> brickList = new ArrayList<Brick>();
    private RectF surface;
    Score score = new Score(0,0);
    boolean gameOver = true;
    boolean firstStart = true;
    private int stage;
    private int totalBrickDestroyed;


    void start(){

        if (firstStart){ stage=1;}
        bricks = new Brick[200];
        result = new Result();
        brickList = new ArrayList<Brick>();
        score = new Score(0,0);
        firstTime = true;
        gameOver = false;
        firstStart = false;
        totalBrickDestroyed =0;
    }

    protected void onDraw(Canvas canvas) {
        if (firstTime) {

            firstTime = false;
            maxX = canvas.getWidth();
            maxY = canvas.getHeight();
            midX= maxX/ 2;
            midY= maxY/ 2;
            move= maxX/10;
            ballX = midX;
            ballY = midY;
            barX=0;
            score.setLife(3);
            score.setPoint(0);


            createSurface();
            createBricks(stage);
            createBall();
            createBar();
        }

        //moving bar ball

        if (!gameOver) {
            ball.moveBall(maxX, maxY);

        }
        //canvas setup
        canvas.drawRGB(255, 255, 255);
        paint.setColor(Color.GREEN);
        paint.setStyle(Paint.Style.FILL);

        //draw Scorecard
        paint.setColor(Color.parseColor("#354458"));
        canvas.drawRect(0, 0, maxX, 40, paint);
        paint.setColor(Color.WHITE);
        paint.setTextSize(maxX/40);
        canvas.drawText("STAGE: "+stage, 20, 35, paint);
        canvas.drawText("LIFE: " + score.getLife(), midX - 150, 60, paint);
        canvas.drawText("POINT: " + score.getPoint(), maxX - 250, 60, paint);

        //draw ball
        paint.setColor(Color.parseColor("#F4782F"));
        canvas.drawOval(ball.getBall(), paint);

        //draw bar paint.setColor(Color.parseColor("#354458"));
        paint.setColor(Color.parseColor("#333333"));
        canvas.drawRect(bar.getBar(), paint);

        //draw brick
        for(int i = 0; i < numBricks; i++){
            if(bricks[i].isBrickVisible) {
                if (brickList.get(i) != null)  {
                    paint.setColor(bricks[i].getBrickColor());
                    canvas.drawRect(brickList.get(i).getBrick(), paint);
                }
            }
        }

        //draw Result
        isGameOver();
        if (gameOver) {
            paint.setColor(Color.WHITE);
            canvas.drawRect(midX - 300, midY - 300, midX + 300, midY + 300, paint);
            paint.setColor(Color.rgb(30, 80, 195));
            if (!firstStart) {
                paint.setTextSize(100);
                canvas.drawText(result(), midX - 200, midY - 180, paint);
                canvas.drawText("Total: " + score.getTotalScore(), midX - 220, midY, paint);

                paint.setTextSize(maxX/9);
                canvas.drawText("Touch To Play", midX - 250, midY + 200, paint);
            }
            else {
                paint.setTextSize(75);
                canvas.drawText("Touch To Start", midX - 250, midY, paint);

            }
        }
        ballDropped();
        checkCollisionBrick();
        checkCollisionBar();

        invalidate();

    }

    boolean isGameOver(){
        result();
        return gameOver;
    }

    public void createBricks(int stage){
        int brickWidth = (int) (maxX/5);
        int brickHeight = (int) (maxY/20);

        numBricks = 0;

        switch (stage) {


            case 1:
                // Stage 1
                for (int column = 0; column < 6; column++) {

                    for (int row = 1; row < 8; row++) {

                        bricks[numBricks] = new Brick(row, column, brickWidth, brickHeight, stage);
                        brickList.add(bricks[numBricks]);
                        numBricks++;





                    }
                }
                break;
            case 2:
                //Stage 2
                for (int column = 0; column < 6; column++) {
                    for (int row = 1; row < 6; row++) {
                        bricks[numBricks] = new Brick(row, column, brickWidth, brickHeight,stage);
                        brickList.add(bricks[numBricks]);
                        numBricks++;
                    }
                }
                break;

            default:
                // Stage 1
                stage =1;
                for (int column = 0; column < 5; column++) {
                    for (int row = 1; row < 5; row++) {

                        bricks[numBricks] = new Brick(row, column, brickWidth, brickHeight, stage);
                        brickList.add(bricks[numBricks]);
                        numBricks++;
                    }
                }
                break;
        }

    }


    public void createBall(){
        ball = new Ball(ballX,ballY,50);
    }

    public void createBar(){
        bar = new Bar(barX+(maxX/2)-175,maxY-50,barX+(maxX/2)+175,maxY-15);

    }

    public void createSurface (){
        surface  = new RectF(0,maxY,maxX,maxY);
    }
    public boolean checkCollisionBar(){


        if (RectF.intersects(ball.getBall(),bar.getBar())){


            ball.moveBallFromBar(bar);

            return true;
        }
        return false;
    }

    public void checkCollisionBrick(){
        for (int i=0;i<numBricks;i++){
            if (RectF.intersects(ball.getBall(),bricks[i].getBrick()) && bricks[i].isBrickVisible) {
                bricks[i].changeBrickColor();

                Log.d("BC:", String.valueOf(bricks[i].getBrickColor()));

                if (bricks[i].bColor == 0)
                {
                    Log.d("BCW:",String.valueOf(bricks[i].getBrickColor()));
                    totalBrickDestroyed++;

                    bricks[i].isBrickVisible = false;
                }
                ball.moveBallFromBrick();
                score.setPoint(10);

            }
        }


    }

    void ballDropped (){

        if (RectF.intersects(ball.getBall(),surface)){
            score.setLife(-1);
            createBall();
        }

    }
    String result(){
        StringBuilder msg = new StringBuilder();
        msg.append("");
        if (score.getLife()!=0 && totalBrickDestroyed == numBricks && !gameOver)
        {
            gameOver =true;
            msg.append("You Win!!!");
            stage++;
            if (stage >2){
                stage=1;
            }

            Log.d("Stage",String.valueOf(stage));

            //msg.append(result.showMsgWin()).append(String.valueOf(result.showMsgResult())).append(String.valueOf(score.getTotalScore()));
        }
        if (score.getLife() == 0) {

            gameOver = true;
            msg.append("You Lost!");
            // msg.append(result.showMsgLost());

        }

        return msg.toString();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (gameOver){
            start();
            gameOver = false;
        }
        float touchX = event.getX();
        if(maxX/2 < touchX  ){

            Log.d("if",String.valueOf(bar.right));
            bar.moveBar(maxX,"right",+move);
        }
        else if(maxX/2 > touchX ){
            Log.d("elseif",String.valueOf(bar.left));
            bar.moveBar(maxX,"left",-move);
        }

        return super.onTouchEvent(event);
    }



    public GameCanvas(Context context) {
        super(context);

        paint = new Paint();

    }

}
