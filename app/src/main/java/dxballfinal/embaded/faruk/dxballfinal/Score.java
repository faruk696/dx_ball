package dxballfinal.embaded.faruk.dxballfinal;

public class Score {

    private int life=0;
    private int point=0;
    public Score (int life, int point)
    {
        this.life = life;
        this.point = point;
    }

    public String getTotalScore(){
        int res=0;
        if (point !=0)
            res= point+100;
        return String.valueOf(res);
    }
    public void setPoint(int pt){
        point = point+pt;
    }
    public void setLife(int lf){
        life = life + lf;
    }
    public int getLife(){
        return life;
    }


    public int getPoint(){
        return point;
    }
}
