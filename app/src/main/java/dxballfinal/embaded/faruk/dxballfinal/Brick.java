package dxballfinal.embaded.faruk.dxballfinal;

import android.graphics.Color;
import android.graphics.RectF;


public class Brick extends RectF {

    int bColor=0;
    int brickColor=0;
    int brickColorList[]= {Color.WHITE, Color.parseColor("#2469A8"),Color.parseColor("#260126")};
    private RectF rect;

    public boolean isBrickVisible;

    public Brick(int row, int column, int width, int height ,int stage){

        isBrickVisible = true;
        int padding = 1;
        brickColor = brickColorList[stage];
        rect = new RectF(column * width + padding,
                row * height + padding,
                column * width + width - padding,
                row * height + height - padding);
        bColor =stage;

    }

    public void changeBrickColor() {
        bColor--;
        brickColor = brickColorList[bColor];
    }

    public int getBrickColor(){
        return brickColor;
    }
    public RectF getBrick() {
        return rect;
    }
}
