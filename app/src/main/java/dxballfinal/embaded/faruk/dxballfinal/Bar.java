package dxballfinal.embaded.faruk.dxballfinal;

import android.graphics.RectF;

public class Bar extends RectF {
    private RectF bar;

    float barLeft,barRight;

    public Bar (float left, float top, float right, float bottom){

        bar = new RectF(left,top,right,bottom);
    }


    public RectF getBar() {
        return bar;
    }

    public float getBarCenter(){
        return bar.centerX();
    }
    public void moveBar (float maxX , String dir , float move){

        if (dir == "left"){
            if ((barLeft > move) && (bar.left+move > 0) ) {

                bar.left += move;
                bar.right += move;
                barLeft = bar.left;
                barRight = bar.right;

            }
            else if(move>=barLeft || bar.left+move<0)
            {

                bar.right=barRight-barLeft;
                bar.left=0;
                barLeft = bar.left;
                barRight = bar.right;


            }
        }

        else if (dir == "right"){
            if( (barRight < (maxX - move))&&  (bar.right < maxX)){

                bar.left += move;
                bar.right += move;
                barLeft = bar.left;
                barRight = bar.right;
            }
            else if((barRight >= (maxX - move)) || bar.right+move>maxX)
            {
                bar.left=maxX-barRight+barLeft;
                bar.right=maxX;
                barLeft = bar.left;
                barRight = bar.right;

            }

        }
    }
}