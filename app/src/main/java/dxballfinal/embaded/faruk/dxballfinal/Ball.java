package dxballfinal.embaded.faruk.dxballfinal;

import android.graphics.RectF;

public class Ball {
    RectF ball;

    float speedY = 10, speedX = 10;

    public Ball(float centerX,float centerY, float radius) {

        ball = new RectF(centerX-radius,centerY-radius,centerX+radius,centerY+radius);


    }


    public RectF getBall() {
        return ball;
    }
    protected void moveBall(float maxX,float maxY) {

        if (ball.top <  120 || ball.bottom > maxY  ) {
            speedY = -speedY;
        }

        if ( ball.left < 5  || ball.right > maxX ) {
            speedX = -speedX;
        }

        ball.left += speedX;
        ball.top += speedY;
        ball.right += speedX;
        ball.bottom += speedY;


    }
    protected void moveBallFromBrick () {


        speedY = -speedY;
        ball.left += speedX;
        ball.top += speedY;
        ball.right += speedX;
        ball.bottom += speedY;


    }

    protected void moveBallFromBar (Bar bar) {

        if (ball.centerX() < bar.getBarCenter()) {
            speedY = -speedY;

            ball.left += speedX;
            ball.top += speedY;
            ball.right += speedX;
            ball.bottom += speedY;
        }

        if (ball.centerX() > bar.getBarCenter()){

            speedY = -speedY;
            speedX = -speedX;

            ball.left += speedX;
            ball.top += speedY;
            ball.right += speedX;
            ball.bottom += speedY;
        }
    }


}